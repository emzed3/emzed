emzed end user API
======================

.. autosummary::
   emzed.Table.create_table

.. automodule:: emzed
   :members: Table, RtType, MzType
   :undoc-members:
   :show-inheritance:
   :noindex:
