#! /bin/sh
#
# build_docs.sh
# Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

. venv/bin/activate
#phinx-apidoc --implicit-namespaces -e -o docs -f src/emzed
cd docs
sphinx-build -M clean . _build
sphinx-build -M html . _build
