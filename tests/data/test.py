#!/usr/bin/env python

import os

from test_fun import test

import emzed
from emzed.core.multiprocessing import multiprocessing


def run():
    file_names = ("test.mzXML",)

    pm_list = []
    for filename in file_names:
        pm_list.append(emzed.PeakMap.load(filename, target_db_file=filename + ".db"))

    with multiprocessing.Pool(2) as pool:
        results = pool.map(test, pm_list)

    for filename in file_names:
        os.unlink(filename + ".db")

    return results


if __name__ == "__main__":
    print(run())
