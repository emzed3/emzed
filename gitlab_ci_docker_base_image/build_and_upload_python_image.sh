set -e
SERVER_URL=sissource.ethz.ch:5005
REPOSITORY_URL=${SERVER_URL}/sispub/emzed/emzed
TAG=${REPOSITORY_URL}/python-with-tox

docker login ${SERVER_URL}

docker build --squash -t ${TAG} .
docker push ${TAG}
