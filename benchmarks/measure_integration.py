#!/usr/bin/env python

import time

import emzed

s = time.time()


def f(t, s):
    print("start f")
    time.sleep(3)
    print("done f")


if __name__ == "__main__":
    import os

    if not os.path.exists("peaks.table"):
        pm = emzed.io.load_peak_map("test.mzXML")
        t = emzed.run_feature_finder_metabo(pm, run_feature_grouper=False)
        t.save("peaks.table")
    else:
        t = emzed.io.load_table("peaks.table")

    N = 2
    # t = emzed.Table.stack_tables([t] + [t] * (1 * N - 1))
    print(len(t))
    # t = t.consolidate("table", overwrite=True)
    emzed.quantification.integrate(t, "sgolay", n_cores=N)
    print(len(t))
    print(time.time() - s)
