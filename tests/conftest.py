# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.


import contextlib
import os
import random
import re
import shutil
import string
import sys
from functools import partial
from pprint import pprint

import matplotlib
import pandas as pd
import pytest
import pytest_regtest

if True:
    # 'if True' avoids resorting by isort, setting the matplotlib backend must happend
    # before importing emzed on my mac, because my local python is not compiled with
    # tkinter support:
    matplotlib.use("Agg")

    from emzed import MzType, PeakMap, RtType, Table
    from emzed.utils.sqlite import Connection


pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)
pd.set_option("display.width", 1000)


@pytest_regtest.register_converter_pre
def fix_temp_sqlite_db_name(output, regtest):
    if sys.platform == "win32":
        return re.sub(r"[a-zA-Z]+\.table", r"<tmp_sqlite_file>", output)
    else:
        return re.sub(r"[a-zA-Z]+\.table", r"<tmp_sqlite_file>", output)


@pytest_regtest.register_converter_pre
def fix_uuid(output, regtest):
    output = re.sub(r'"[0-9a-f]{32}"', '"<uuid>"', output)
    return re.sub(r"'[0-9a-f]{32}'", "'<uuid>'", output)


@pytest_regtest.register_converter_pre
def fix_uuid_in_tables(output, regtest):
    # {peakmap,data}_8f2d8be73ba2259076fe966aa797018a_?* etc:
    output = re.sub(r"_[0-9a-f]{32}_", "_<uuid>_", output)
    output = re.sub(r"_[0-9a-f]{32}\b", "_<uuid>", output)
    return output


@pytest.fixture(scope="session")
def here():
    yield os.path.dirname(os.path.abspath(__file__))


@pytest.fixture(scope="session")
def data_path(here):
    yield partial(os.path.join, here, "data")


@pytest_regtest.register_converter_post
def fix_path_to_data_folder(output, request):
    lines = []

    sep = os.path.sep
    for line in output.split("\n"):
        line = re.sub(
            rf"([CD]:)?(\{sep}[^\{sep}]*?)+\{sep}tests\{sep}data\{sep}",
            "<test_data>/",
            line,
        )
        lines.append(line)
    return "\n".join(lines)


@pytest_regtest.register_converter_post
def fix_table_id(output, request):
    lines = []

    for line in output.split("\n"):
        line = re.sub(r"<Table [a-z0-9]{6}\.{3}>", "<Table xxxxxx...>", line)
        lines.append(line)
    return "\n".join(lines)


@pytest_regtest.register_converter_post
def fix_peakmap_id(output, request):
    lines = []

    for line in output.split("\n"):
        line = re.sub(r"<PeakMap [a-z0-9]+\.*>", "<PeakMap xxxxx>", line)
        lines.append(line)
    return "\n".join(lines)


@pytest.fixture
def pp(regtest):
    def _pp(*a):
        if a:
            pprint(*a, stream=regtest)
        else:
            print(file=regtest)

    yield _pp


@pytest.fixture(scope="function")
def pm(data_path):
    pm = PeakMap.load(data_path("test_smaller.mzXML"))
    pm.meta_data["id"] = "pm"
    yield pm


@pytest.fixture(scope="function")
def pm_on_disk(data_path, tmpdir):
    target_db_file = tmpdir.join("peakmap.db").strpath
    pm = PeakMap.load(data_path("test_smaller.mzXML"), target_db_file=target_db_file)
    pm.meta_data["id"] = "pm_on_disk"
    yield pm


@pytest.fixture(scope="function")
def t0():
    t0 = Table.create_table(
        ["a", "b"], (int, str), rows=[[1, 2], [3, 4], [5, None], [None, 6]]
    )
    yield t0


@pytest.fixture
def dummy_table():
    t = Table.create_table(
        ["a", "b", "c", "mz", "rt"],
        [int, float, str, MzType, RtType],
        rows=[[1, 1.0, "1", 1000.00001, 60.0], [2, 2.0, "2", 600.1, 90.0]],
        meta_data=dict(purpose="testing"),
    )
    yield t


# https://stackoverflow.com/questions/42228895/how-to-parametrize-a-pytest-fixture

# we create a parmeterized fixture here


@pytest.fixture
def create_table(request, tmpdir):
    if hasattr(request, "param"):
        # create_table fixture called within paremetrized test
        if request.param not in ("memory", "disk"):
            raise ValueError(f"can not handle param {request.param!r}")

        if request.param == "disk":
            # create fresh table db in temp folder for every call:
            def create_table(*a, **kw):
                file_name = _random_string()
                path = tmpdir.join(f"{file_name}.table").strpath
                return Table.create_table(*a, **kw, path=path)

            return create_table

        else:
            return Table.create_table

    # no paremerized test, create_table fixture called directly
    return Table.create_table


def _random_string(n=20):
    return "".join(random.sample(string.ascii_letters, n))


with_mem_and_disk = pytest.mark.parametrize(
    "create_table", ["memory", "disk"], indirect=True
)

with_mem_only = pytest.mark.parametrize("create_table", ["memory"], indirect=True)

with_disk_only = pytest.mark.parametrize("create_table", ["disk"], indirect=True)


@pytest.fixture
def debug_sql(regtest):
    @contextlib.contextmanager
    def debug():
        Connection._debug = regtest.write
        try:
            yield
        finally:
            Connection._debug = None

    yield debug


@pytest.fixture(scope="session", autouse=True)
def mock_terminal_size():
    def default_screen_size(*a, **kw):
        return (180, 50)

    # pytest mocking does not work on session fixtures, so we did this
    # ourselves:
    orig = shutil.get_terminal_size
    shutil.get_terminal_size = default_screen_size
    yield
    shutil.get_terminal_size = orig
