#! /usr/bin/env python
# Copyright 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>

import numpy as np

import emzed

try:
    profile
except NameError:
    profile = lambda f: f

values = np.linspace(100, 200, 100_000)

t = emzed.to_table("mz", values, emzed.MzType)
t2 = t.copy()


atol = 0.001


@profile
def run_join():
    t.join(t2, (t2.mz - t.mz).abs() < atol)


@profile
def run_join_fast(t2=t2, t=t):
    t = t.copy()
    t.add_column("bin", (t.mz / atol).floor(), int)
    t.add_column("bin1", t.bin + 1, int)
    t.add_column("bin2", t.bin - 1, int)
    t._model._conn.execute("CREATE INDEX _ix_bin1 ON data(col_1);")
    t._model._conn.execute("CREATE INDEX _ix_bin2 ON data(col_2);")
    t._model._conn.execute("CREATE INDEX _ix_bin3 ON data(col_3);")
    tx = t.join(
        t2,
        (
            (t.bin == (t2.mz / atol).floor())
            | (t.bin1 == (t2.mz / atol).floor())
            | (t.bin2 == (t2.mz / atol).floor())
        )
        & t2.mz.approx_equal(t.mz, atol=atol, rtol=0),
    )
    tx.drop_columns("bin", "bin1", "bin2")
    print(len(tx))


@profile
def run_join_fast_2(t2=t2, t=t):
    t = t.copy()
    tx = t.fast_join(t2, "mz", atol=atol)
    print(len(tx))


@profile
def run_join_fast_3(t2=t2, t=t):
    t = t.copy()
    # no index
    tx = t.join(t2, t.mz == t2.mz)
    # with index
    tx = t.fast_join(t2, "mz", atol=0, rtol=0)
    print(len(tx))


# run_join()
run_join_fast()
run_join_fast_2()
run_join_fast_3()
