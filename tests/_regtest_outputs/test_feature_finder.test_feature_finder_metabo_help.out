calls pyopenms MassTraceDetection + FeatureFindingMetabo

:param peak_map: ``emzed.PeakMap``
:param ms_level: optional specification of ms_level
:param verbose: set to ``False`` to suppress output
:param run_feature_finder_metabo: set to ``False`` to suppress grouping of peaks to features

Common Parameters:

:param common_chrom_peak_snr: 

    Default value: ``3.0``.

:param common_chrom_fwhm: 

    Default value: ``5.0``.

Parameters Mass Trace Detector:

:param mtd_mass_error_ppm: Allowed mass deviation (in ppm).

    Default value: ``20.0``.

:param mtd_noise_threshold_int: Intensity threshold below which peaks are removed as noise.

    Default value: ``10.0``.

:param mtd_reestimate_mt_sd: Enables dynamic re-estimation of m/z variance during mass trace collection stage.

    Default value: ``'true'``, allowed values: ``'true'``, ``'false'``.

:param mtd_quant_method: Method of quantification for mass traces. For LC data 'area' is recommended, 'median' for direct injection data. 'max_height' simply uses the most intense peak in the trace.

    Default value: ``'area'``, allowed values: ``'area'``, ``'median'``, ``'max_height'``.

:param mtd_trace_termination_criterion: Termination criterion for the extension of mass traces. In 'outlier' mode, trace extension cancels if a predefined number of consecutive outliers are found (see trace_termination_outliers parameter). In 'sample_rate' mode, trace extension in both directions stops if ratio of found peaks versus visited spectra falls below the 'min_sample_rate' threshold.

    Default value: ``'outlier'``, allowed values: ``'outlier'``, ``'sample_rate'``.

:param mtd_trace_termination_outliers: Mass trace extension in one direction cancels if this number of consecutive spectra with no detectable peaks is reached.

    Default value: ``5``.

:param mtd_min_sample_rate: Minimum fraction of scans along the mass trace that must contain a peak.

    Default value: ``0.5``.

:param mtd_min_trace_length: Minimum expected length of a mass trace (in seconds).

    Default value: ``5.0``.

:param mtd_max_trace_length: Maximum expected length of a mass trace (in seconds). Set to a negative value to disable maximal length check during mass trace detection.

    Default value: ``-1.0``.

Elution Peak Detection:

:param epdet_width_filtering: Enable filtering of unlikely peak widths. The fixed setting filters out mass traces outside the [min_fwhm, max_fwhm] interval (set parameters accordingly!). The auto setting filters with the 5 and 95% quantiles of the peak width distribution.

    Default value: ``'fixed'``, allowed values: ``'off'``, ``'fixed'``, ``'auto'``.

:param epdet_min_fwhm: Minimum full-width-at-half-maximum of chromatographic peaks (in seconds). Ignored if parameter width_filtering is off or auto.

    Default value: ``3.0``.

:param epdet_max_fwhm: Maximum full-width-at-half-maximum of chromatographic peaks (in seconds). Ignored if parameter width_filtering is off or auto.

    Default value: ``60.0``.

:param epdet_masstrace_snr_filtering: Apply post-filtering by signal-to-noise ratio after smoothing.

    Default value: ``'false'``, allowed values: ``'true'``, ``'false'``.

Parameters Feature Finding Metabo:

:param ffm_local_rt_range: RT range where to look for coeluting mass traces

    Default value: ``10.0``.

:param ffm_local_mz_range: MZ range where to look for isotopic mass traces

    Default value: ``6.5``.

:param ffm_charge_lower_bound: Lowest charge state to consider

    Default value: ``1``.

:param ffm_charge_upper_bound: Highest charge state to consider

    Default value: ``3``.

:param ffm_report_summed_ints: Set to true for a feature intensity summed up over all traces rather than using monoisotopic trace intensity alone.

    Default value: ``'false'``, allowed values: ``'false'``, ``'true'``.

:param ffm_enable_RT_filtering: Require sufficient overlap in RT while assembling mass traces. Disable for direct injection data..

    Default value: ``'true'``, allowed values: ``'false'``, ``'true'``.

:param ffm_isotope_filtering_model: Remove/score candidate assemblies based on isotope intensities. SVM isotope models for metabolites were trained with either 2% or 5% RMS error. For peptides, an averagine cosine scoring is used. Select the appropriate noise model according to the quality of measurement or MS device.

    Default value: ``'metabolites (5% RMS)'``, allowed values: ``'metabolites (2% RMS)'``, ``'metabolites (5% RMS)'``, ``'peptides'``, ``'none'``.

:param ffm_mz_scoring_13C: Use the 13C isotope peak position (~1.003355 Da) as the expected shift in m/z for isotope mass traces (highly recommended for lipidomics!). Disable for general metabolites (as described in Kenar et al. 2014, MCP.).

    Default value: ``'false'``, allowed values: ``'false'``, ``'true'``.

:param ffm_use_smoothed_intensities: Use LOWESS intensities instead of raw intensities.

    Default value: ``'true'``, allowed values: ``'false'``, ``'true'``.

:param ffm_report_convex_hulls: Augment each reported feature with the convex hull of the underlying mass traces (increases featureXML file size considerably).

    Default value: ``'true'``, allowed values: ``'false'``, ``'true'``.

:param ffm_report_chromatograms: Adds Chromatogram for each reported feature (Output in mzml).

    Default value: ``'false'``, allowed values: ``'false'``, ``'true'``.

:param ffm_remove_single_traces: Remove unassembled traces (single traces).

    Default value: ``'false'``, allowed values: ``'false'``, ``'true'``.

:param ffm_mz_scoring_by_elements: Use the m/z range of the assumed elements to detect isotope peaks. A expected m/z range is computed from the isotopes of the assumed elements. If enabled, this ignores 'mz_scoring_13C'

    Default value: ``'false'``, allowed values: ``'false'``, ``'true'``.

:param ffm_elements: Elements assumes to be present in the sample (this influences isotope detection).

    Default value: ``'CHNOPS'``, allowed values: .


:returns: `emzed.Table` with columns feature_id, mz, mzmin, mzmax, rt, rtmin, rtmax, intensity, quality, fwhm, and z.
