# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.


import matplotlib
import numpy as np
import pylab as pl

matplotlib.rcParams["font.sans-serif"] = ["Optima"]

x = np.arange(0, 12, 0.01)


def make_bump(center, w=0.45, x=x):
    return np.exp(-(x - center) * (x - center) / w)


y = make_bump(1.0)
y += 0.6 * make_bump(3.0)
y += 0.45 * make_bump(5.0)
y += 0.3 * make_bump(7.0)

y *= 0.9

(lines,) = pl.plot(x + 0.25, y - 0.045, "#000000", linewidth=10, alpha=0.7)
(lines,) = pl.plot(x + 0.15, y - 0.03, "#ffffff", linewidth=10)
lines.set_antialiased(True)
(lines,) = pl.plot(x, y, "#0069b4", linewidth=12)
lines.set_antialiased(True)
ax = pl.gca()
pl.box(False)
ax.yaxis.set_visible(False)
ax.xaxis.set_visible(False)
txt = pl.text(
    2.7, 0.72, "emzed 3", fontsize=72, alpha=0.8, family="sans-serif"
)  # style="oblique")
txt.set_weight("heavy")

pl.savefig("logo.png")  # , transparent=True)
