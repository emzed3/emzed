#! /bin/sh
#
# cleanup_code.sh
# Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

echo 'remove breakpoint() calls'
find src tests -name "*.py" | xargs -n 1 sed -i "" -e "/^ *breakpoint() *$/d"

echo 'remove @profile decorators'
find src tests -name "*.py" | xargs -n 1 sed -i "" -e "/^ *@profile *$/d"
