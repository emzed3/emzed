#! /bin/sh
#
# open_docs.sh
# Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

python -m webbrowser -t  /home/pkiefer/emzed/docs/_build/html/index.html
