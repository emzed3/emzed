# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.


import os
import sys

for key, value in sorted(os.environ.items()):
    print(f"{key} = {value}")


def is_ci():
    return os.environ.get("CI") is not None


# tests below from https://docs.python.org/3/library/sys.html#sys.platform


def is_linux_ci():
    return is_ci() and sys.platform.startswith("linux")


def is_windows_ci():
    return is_ci() and sys.platform == "win32"


print("CI checks:", is_linux_ci(), is_windows_ci())
