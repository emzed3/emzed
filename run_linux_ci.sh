#!/usr/bin/env sh
gitlab-runner exec docker --env SLEEP=0 --timeout 99999 ${1:-ubuntu_20_04}
