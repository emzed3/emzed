.PHONY: help clean clean-pyc clean-build list test test-all coverage docs release sdist

help:
	@echo
	@echo "check       - check code with flake8"
	@echo "style-check - run tests quickly with the default Python"
	@echo "test-all    - run tests on every Python version with tox"
	@echo "coverage    - check code coverage quickly with the default Python"
	@echo "docs        - generate Sphinx HTML documentation, including API docs"
	@echo "build_docs  - builds html documentation"
	@echo "deploy_docs - deploys docs at web server"
	@echo "sdist       - package"
	@echo "upload_sis  - uploads package to pypi-sissource.ethz.ch"

clean: clean-build clean-pyc

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -rf {} +

style-check:
	git ls-tree HEAD -r --name-only\
		| grep '^\(setup\.py\|src\|tests\|benchmarks\)'\
		| grep '\.py$$'\
		| xargs ruff check --line-length=88 --ignore E731,E741

test:
	py.test tests

test-all:
	tox

coverage:
	coverage run --source src -m pytest tests
	coverage report -m
	coverage html
	open htmlcov/index.html

build_docs:
	sphinx-apidoc --implicit-namespaces -e -o docs -f src/emzed src/emzed/remote_package/client.py
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	# cp -R htmlcov docs/_build/html

update_docs:
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	# cp -R htmlcov docs/_build/html

deploy_docs: update_docs
	scp -r docs/_build/html/* w3_emzed2@emzed.ethz.ch:htdocs/3/emzed


docs: build_docs
	open docs/_build/html/index.html

sdist: clean
	python setup.py sdist
	ls -l dist

upload_sis: sdist
	twine upload -r sissource dist/*
