#!/bin/sh
#
# reformat_code.sh
# Copyright (C) 2019 Uwe Schitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

echo
echo RUN ISORT
echo
isort --skip-gitignore src/emzed tests benchmarks

echo
echo RUN RUFF FORMAT
echo
ruff format src/emzed tests benchmarks
