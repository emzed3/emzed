How to install emzed?
=====================

We host the Python package own package server, which will change after
publication.

Before you install `emzed3` make sure that you use the latest version of `pip`.

.. code-block:: bash

    $ pip install emzed

We also highly recommended to use a virtual environment for setting up ``emzed``.



Installation on HPC cluster Euler of ETH Zurich
-----------------------------------------------

The following instructions create a virtual environment with ``emzed3`` installed:

.. code-block:: bash

    $ module purge
    $ module load new
    $ module load python/3.7.1
    $ module unload gcc
    $ module load llvm/7.0.1
    $ module load eth_proxy

    $ python3 -m venv venv_emzed
    $ source venv_emzed/bin/activate

    $ pip install -U pip
    $ pip install emzed3
