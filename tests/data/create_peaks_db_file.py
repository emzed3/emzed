#! /usr/bin/env python
# Copyright 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>

from emzed import PeakMap, run_feature_finder_metabo

pm = PeakMap.load("test_smaller.mzXML")

table = run_feature_finder_metabo(pm, mtd_noise_threshold_int=100_000)

table.save("peaks.table", overwrite=True)
