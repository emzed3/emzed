.. emzed documentation master file, created by
   sphinx-quickstart on Fri Aug  2 11:02:04 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to emzed's documentation!
=================================

*emzed* makes developing analysis strategies for LCMS data as simple as possible.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   installation
   api
   emzed



`HTML test coverage report <./htmlcov/index.html>`_




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
