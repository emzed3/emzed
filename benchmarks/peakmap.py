# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.


import multiprocessing
import os
import random
import tempfile
import time

from emzed import PeakMap

random.seed(42)


here = os.path.dirname(os.path.abspath(__file__))

TMP_FOLDER = tempfile.mkdtemp()

DB_FILE = os.path.join(TMP_FOLDER, "peaks.db")
MZXML_FILE = os.path.join(here, "../tests/data/test.mzXML")


def execute(args):
    function, _args = args
    return function(*_args)


def main():
    print(f"READ {MZXML_FILE} AND WRITE TO {DB_FILE}")
    print()

    if os.path.exists(DB_FILE):
        os.remove(DB_FILE)

    started = time.time()

    pm = PeakMap.load(MZXML_FILE, target_db_file=DB_FILE)

    needed = time.time() - started

    print("loading {} spectrea needed {:.1f} seconds".format(len(pm), needed))

    started = time.time()

    for spectrum in pm.spectra:
        spectrum.peaks.shape

    needed = time.time() - started

    print(
        "fetching all peaks from {} spectrea needed {:.1f} seconds".format(
            len(pm), needed
        )
    )

    rt_min, rt_max = pm.rt_range()
    mz_min, mz_max = pm.mz_range()

    windows = []
    mz_width = 0.05

    for _ in range(500):
        rtlen = random.uniform(5, 20)
        rtmi = random.uniform(rt_min, rt_max - rtlen)
        rtma = rtmi + rtlen

        mzmi = random.uniform(mz_min, mz_max - mz_width)
        mzma = mzmi + mz_width

        windows.append((mzmi, mzma, rtmi, rtma, 1))

    started = time.time()
    lens = []
    for rt_min, rt_max, mz_min, mz_max, ms_level in windows:
        rts, intensities = pm.chromatogram(rt_min, rt_max, mz_min, mz_max, ms_level)
        assert len(rts) == len(intensities)
        lens.append(len(rts))
    needed = time.time() - started
    print("{} chromatograms needed {:.3f} seconds".format(len(windows), needed))
    print(lens)

    def chromatogram(window):
        return pm.chromatogram(*window)

    started = time.time()
    with multiprocessing.Pool(4) as p:
        chromatograms = p.map(execute, [(pm.chromatogram, w) for w in windows])
        lens = [len(rts) for (rts, iis) in chromatograms]
    needed = time.time() - started
    print(
        "{} chromatograms with 4 workers needed {:.3f} seconds".format(
            len(windows), needed
        )
    )
    print(lens)


if __name__ == "__main__":
    main()
