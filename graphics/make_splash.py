# This file is part of emzed (https://emzed.ethz.ch), a software toolbox for analysing
# LCMS data with Python.
#
# Copyright (C) 2020 ETH Zurich, SIS ID.
#
# This program is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this
# program.  If not, see <http://www.gnu.org/licenses/>.


import matplotlib
import numpy as np
import pylab as plt

matplotlib.rcParams["font.sans-serif"] = ["Optima"]

x = np.arange(0, 13, 0.01)


def make_bump(center, w=0.45, x=x):
    return np.exp(-(x - center) * (x - center) / w)


y = make_bump(1.0)
y += 0.6 * make_bump(3.0)
y += 0.45 * make_bump(5.0)
y += 0.3 * make_bump(7.0)

y *= 0.9

fig = plt.figure()
fig.set_facecolor("b")

(lines,) = plt.plot(x + 0.20, y - 0.040, "#000000", linewidth=9, alpha=0.4)
(lines,) = plt.plot(x + 0.12, y - 0.025, "#ffffff", linewidth=9)
lines.set_antialiased(True)
(lines,) = plt.plot(x, y, "#0069b4", linewidth=12)
lines.set_antialiased(True)
ax = plt.gca()
ax.set_facecolor("#f7f7f7")
ax.yaxis.set_visible(False)
ax.xaxis.set_visible(False)
txt = plt.text(
    2.72, 0.72, "emzed 3", fontsize=72, alpha=0.8, family="sans-serif"
)  # style="oblique")

txt = plt.text(
    5.01, 0.57, "© 2019 SIS ETH Zürich", fontsize=20, alpha=0.6, family="sans-serif"
)  # style="oblique")


plt.box(False)
plt.savefig("splash.png", dpi=200)  # , transparent=True)
